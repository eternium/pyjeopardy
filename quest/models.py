# -*- coding: utf-8 -*-
# todo: добавить раунды игр
# todo: добавить факт показа вопроса

from django.db import models

#class GameRound(models.Model):

class Category(models.Model):
	category_name = models.CharField(max_length=100, default="CatName", null=True)
	gameround = models.PositiveIntegerField(default=0)

	def __unicode__(self):
		return self.category_name

	def to_json(self):
		return {
			"name": self.category_name,
			"questions": map(lambda x:x.to_json(), self.question_set.order_by('question_price').all())
		}

class Question(models.Model):
	question_text = models.CharField(max_length=2000)
	question_price = models.IntegerField(default=1)
	category = models.ForeignKey(Category)
	is_viewed = models.BooleanField(default=False)

	def __unicode__(self):
		return self.question_text

	def to_json(self):
		return {"id": self.id, "cost": self.question_price, "text": self.question_text, "played": self.is_viewed}
