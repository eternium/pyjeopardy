from django.contrib import admin
from .models import Category, Question

class QuestionInLine(admin.StackedInline):
	model=Question


class CategoryAdmin(admin.ModelAdmin):
	filedsets = [
		('Category parameters', {'fields': ['category_name', 'gameround']}),
		('Questions', {'fields': ['question_text', 'questoin_price']}),
	]

	inlines = [QuestionInLine]

admin.site.register(Category, CategoryAdmin)
admin.site.register(Question)
