from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^games/(?P<game_round>[0-99]+)/$', views.getQuestionsTable, name='getQuestionsTable'),
]