# -*- coding: utf-8 -*-

from django.shortcuts import render, render_to_response
from django.http import HttpResponse

import json
from .models import Category, Question

def index(request):
    return render_to_response('index.html')

def getQuestionsTable(request, game_round=1):
	roundCategory = Category.objects.filter(gameround=game_round)

	roundName = "Раунд "+str(game_round)

	data = {"name": roundName, \
		"categories": map(lambda x:x.to_json(), roundCategory)\
	}

	return HttpResponse(json.dumps(data), content_type="application/json")