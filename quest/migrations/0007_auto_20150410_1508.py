# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quest', '0006_auto_20150410_1508'),
    ]

    operations = [
        migrations.RenameField(
            model_name='question',
            old_name='is_vieved',
            new_name='is_viewed',
        ),
    ]
