# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quest', '0005_question_is_vievew'),
    ]

    operations = [
        migrations.RenameField(
            model_name='question',
            old_name='is_vievew',
            new_name='is_vieved',
        ),
    ]
