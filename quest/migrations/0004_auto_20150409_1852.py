# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quest', '0003_gameround'),
    ]

    operations = [
        migrations.DeleteModel(
            name='GameRound',
        ),
        migrations.AddField(
            model_name='category',
            name='gameround',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='category',
            name='category_name',
            field=models.CharField(default=b'CatName', max_length=100, null=True),
        ),
    ]
