# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quest', '0002_question_question_price'),
    ]

    operations = [
        migrations.CreateModel(
            name='GameRound',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gameround_name', models.CharField(max_length=100)),
                ('gameround_number', models.IntegerField(default=0)),
            ],
        ),
    ]
