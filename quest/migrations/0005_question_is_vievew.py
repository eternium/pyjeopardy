# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quest', '0004_auto_20150409_1852'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='is_vievew',
            field=models.BooleanField(default=False),
        ),
    ]
