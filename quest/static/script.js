renderAllGames = function(games) {
  var source   = $("#games-template").html();

  var template = Handlebars.compile(source);
  var html    = template({games: games});
  $('.main-content').html(html);
}

renderGameTable = function(game) {
  var source   = $("#entry-template").html();
  var template = Handlebars.compile(source);
  var html    = template(game);
  $('.main-content').html(html);
}

loadGame = function(url) {
  $.ajax({
    url: url,
    data: {},
    success: function(data) {
      var game = data;
      renderGameTable(game);

      all_questions = $.map(game.categories, function(cats, i) {
        return cats.questions;
      });

      for(var i = 0, l = all_questions.length; i < l; i++) {
        question = all_questions[i];
        question_texts[question.id] = question.text;
      }
    },
    error: function(xhr, status, error) {
      alert(status);
    },
    dataType: "json"
  });

}

var question_texts = {};

showQuestion = function(id) {
  $(".main-table").hide();
  var text = question_texts[id];
  $(".big-question_inner").html(text);
  $(".big-question").addClass("active");
}

$(document).ready(function(){
  $("body").on("click", ".question", function(){
    var id = $(this).data("id");
    showQuestion(id);
    $(this).addClass("played");
  });

  $(".big-question").click(function(){
    $(".big-question").removeClass("active");
    $(".main-table").show();
  });

  $("body").on("click", ".get_game", function(){
    url = $(this).attr('href');
    loadGame(url)
    return false;
  })

  games = [1,2,3,4,5];
  renderAllGames(games);
})
