from django.conf.urls import include, url
from django.contrib import admin

from django.views.generic.base import TemplateView

STATIC_ROOT = ""

urlpatterns = [
    #url(r'^$', TemplateView.as_view(template_name='index.html'), name="home"),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('quest.urls')),
]
